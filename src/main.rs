use std::f32::consts::PI;

use macroquad::prelude::*;
use ncollide2d::{
    math::{Isometry, Rotation, Vector},
    query::PointQuery,
    shape::Triangle,
};

const PLAYER_W: f32 = 20.0;
const PLAYER_H: f32 = 20.0;
const ROTSPEED: f32 = 3.0;
const MOVESPEED: f32 = 500.0;
const DAMAGE_MAX: f32 = 3.0;
const DAMAGE_COOLDOWN_PER_S: f32 = 0.5;
const CAN_SEE_LENGTH: f32 = 200.0;
const CAN_SEE_WIDTH: f32 = 100.0;

struct Player {
    position: Vector<f32>,
    angle: f32,
    damage: f32,
}

enum State {
    MainMenu { space_pressed: bool },
    Playing { player1: Player, player2: Player },
    PlayerWon { who: u32 },
}

impl State {
    fn main_menu(space_pressed: Option<bool>) -> State {
        State::MainMenu {
            space_pressed: space_pressed.unwrap_or(false),
        }
    }

    fn playing() -> State {
        State::Playing {
            player1: Player {
                position: Vector::new(50.0, 50.0),
                angle: 3.0 * PI / 2.0,
                damage: 0.0,
            },
            player2: Player {
                position: Vector::new(50.0, 250.0),
                angle: 1.0 * PI / 2.0,
                damage: 0.0,
            },
        }
    }

    fn player_won(who: u32) -> State {
        State::PlayerWon { who }
    }

    fn update(&mut self, delta: f32) {
        match self {
            State::MainMenu { space_pressed } => {
                if *space_pressed {
                    if !is_key_down(KeyCode::Space) {
                        *space_pressed = false;
                    }
                } else if is_key_down(KeyCode::Space) {
                    *self = State::playing()
                }
            }
            State::Playing { player1, player2 } => {
                if is_key_down(KeyCode::A) {
                    player1.angle -= ROTSPEED * delta;
                }
                if is_key_down(KeyCode::D) {
                    player1.angle += ROTSPEED * delta;
                }
                if is_key_down(KeyCode::W) {
                    player1.position +=
                        Rotation::from_angle(player1.angle) * Vector::new(MOVESPEED * delta, 0.0);
                }
                if is_key_down(KeyCode::S) {
                    player1.position -=
                        Rotation::from_angle(player1.angle) * Vector::new(MOVESPEED * delta, 0.0);
                }

                if is_key_down(KeyCode::Left) {
                    player2.angle -= ROTSPEED * delta;
                }
                if is_key_down(KeyCode::Right) {
                    player2.angle += ROTSPEED * delta;
                }
                if is_key_down(KeyCode::Up) {
                    player2.position +=
                        Rotation::from_angle(player2.angle) * Vector::new(MOVESPEED * delta, 0.0);
                }
                if is_key_down(KeyCode::Down) {
                    player2.position -=
                        Rotation::from_angle(player2.angle) * Vector::new(MOVESPEED * delta, 0.0);
                }

                // check collision
                let p1_tri_l = player1.position
                    + Rotation::from_angle(player1.angle)
                        * Vector::new(CAN_SEE_LENGTH, CAN_SEE_WIDTH);
                let p1_tri_r = player1.position
                    + Rotation::from_angle(player1.angle)
                        * Vector::new(CAN_SEE_LENGTH, -CAN_SEE_WIDTH);
                let p1_tri =
                    Triangle::new(player1.position.into(), p1_tri_l.into(), p1_tri_r.into());
                if p1_tri.contains_point(&Isometry::identity(), &player2.position.into()) {
                    player2.damage += delta;
                } else {
                    player2.damage -= DAMAGE_COOLDOWN_PER_S * delta;
                }
                if player2.damage < 0.0 {
                    player2.damage = 0.0;
                }

                let p2_tri_l = player2.position
                    + Rotation::from_angle(player2.angle)
                        * Vector::new(CAN_SEE_LENGTH, CAN_SEE_WIDTH);
                let p2_tri_r = player2.position
                    + Rotation::from_angle(player2.angle)
                        * Vector::new(CAN_SEE_LENGTH, -CAN_SEE_WIDTH);
                let p2_tri =
                    Triangle::new(player2.position.into(), p2_tri_l.into(), p2_tri_r.into());
                if p2_tri.contains_point(&Isometry::identity(), &player1.position.into()) {
                    player1.damage += delta;
                } else {
                    player1.damage -= DAMAGE_COOLDOWN_PER_S * delta;
                }
                if player1.damage < 0.0 {
                    player1.damage = 0.0;
                }

                let p1_damage = player1.damage;
                let p2_damage = player2.damage;

                println!("{:.1}, {:.1}", p1_damage, p2_damage);

                if p1_damage >= DAMAGE_MAX {
                    *self = State::player_won(2);
                }
                if p2_damage >= DAMAGE_MAX {
                    *self = State::player_won(1);
                }
            }
            State::PlayerWon { who: _ } => {
                if is_key_down(KeyCode::Space) {
                    *self = State::main_menu(Some(true));
                }
            }
        }
    }

    fn draw(&self) {
        match self {
            State::MainMenu { space_pressed: _ } => {
                draw_text("time to hide", 20.0, 80.0, 80.0, WHITE);
                draw_text("press space to start", 20.0, 160.0, 80.0, WHITE);
            }
            State::Playing { player1, player2 } => {
                let p1_p = player1.position + Vector::new(PLAYER_W / 2.0, PLAYER_H / 2.0);
                let p1_tri_l = p1_p
                    + Rotation::from_angle(player1.angle)
                        * Vector::new(CAN_SEE_LENGTH, CAN_SEE_WIDTH);
                let p1_tri_r = p1_p
                    + Rotation::from_angle(player1.angle)
                        * Vector::new(CAN_SEE_LENGTH, -CAN_SEE_WIDTH);
                draw_triangle(
                    Vec2::from_array(p1_p.into()),
                    Vec2::from_array(p1_tri_l.into()),
                    Vec2::from_array(p1_tri_r.into()),
                    BLUE,
                );
                let p2_p = player2.position + Vector::new(PLAYER_W / 2.0, PLAYER_H / 2.0);
                let p2_tri_l = p2_p
                    + Rotation::from_angle(player2.angle)
                        * Vector::new(CAN_SEE_LENGTH, CAN_SEE_WIDTH);
                let p2_tri_r = p2_p
                    + Rotation::from_angle(player2.angle)
                        * Vector::new(CAN_SEE_LENGTH, -CAN_SEE_WIDTH);
                draw_triangle(
                    Vec2::from_array(p2_p.into()),
                    Vec2::from_array(p2_tri_l.into()),
                    Vec2::from_array(p2_tri_r.into()),
                    RED,
                );

                draw_rectangle(
                    player1.position[0],
                    player1.position[1],
                    PLAYER_W,
                    PLAYER_H,
                    BLUE,
                );
                draw_rectangle(
                    player2.position[0],
                    player2.position[1],
                    PLAYER_W,
                    PLAYER_H,
                    RED,
                );

                let p1_hp_pos =
                    player1.position + Vector::new(PLAYER_W / 2.0, PLAYER_H / 2.0 - 10.0);
                draw_text(
                    &format!("{:.1}", player1.damage),
                    p1_hp_pos[0],
                    p1_hp_pos[1],
                    20.0,
                    WHITE,
                );

                let p2_hp_pos =
                    player2.position + Vector::new(PLAYER_W / 2.0, PLAYER_H / 2.0 - 10.0);
                draw_text(
                    &format!("{:.1}", player2.damage),
                    p2_hp_pos[0],
                    p2_hp_pos[1],
                    20.0,
                    WHITE,
                );
            }
            State::PlayerWon { who } => {
                draw_text(&format!("player {who} won"), 20.0, 80.0, 80.0, WHITE);
                draw_text(
                    "looks like someone needs to practice",
                    20.0,
                    160.0,
                    60.0,
                    WHITE,
                );
                draw_text(
                    "press space to return to main menu",
                    20.0,
                    220.0,
                    60.0,
                    WHITE,
                );
            }
        }
    }
}

#[macroquad::main("QuickJam")]
async fn main() {
    let mut state = State::main_menu(None);
    loop {
        clear_background(BLACK);
        state.update(get_frame_time());
        state.draw();
        next_frame().await
    }
}
